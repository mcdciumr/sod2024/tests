# Unit Tests in MR-Pipelines

Pipelines können so konfiguriert sein, dass sie nur dann starten, wenn Merge Requests erzeugt
oder geändert werden. Tests, die dann durchlaufen, können den neuen Code auf Korrektheit prüfen
und damit manuelle Prüfungen beschleunigen.

```yaml
image: python:3.12

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

square:
  script:
    - python3 square_test.py
```

- Spezialisierte Docker-Images können geladen werden, um mit vorinstallierter Software zu starten (`python:3.12`).
- Mit **Rules** kann gesteuert werden, ob die Pipeline ausgeführt wird. `$CI_PIPELINE_SOURCE`-Variable gibt an,
  welches Ereignis die Pipeline auslösen darf. 
- Übersicht über CI/CD-Variablen: https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
- Um das Bestehen von Pipelines bei Merge Requests verpflichtend zu machen, muss die Projekteinstellung 
  **Settings -> Merge Requests -> Pipelines must succeed** gesetzt sein. Danach kann ein Merge Request nur
  angenommen werden, wenn alle Pipelines erfolgreich durchlaufen.
