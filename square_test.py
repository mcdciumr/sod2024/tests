import unittest

import square


class TestSquareMethods(unittest.TestCase):

    def test_positive(self):
        self.assertEqual(square.square(0), 0)
        self.assertEqual(square.square(1), 1)
        self.assertEqual(square.square(4), 16)
        self.assertEqual(square.square(12), 144)

    def test_negative(self):
        self.assertEqual(square.square(-0), 0)
        self.assertEqual(square.square(-1), 1)
        self.assertEqual(square.square(-4), 16)

    def test_invalid(self):
        self.assertEqual(square.square("hello"), -1)


if __name__ == '__main__':
    unittest.main()